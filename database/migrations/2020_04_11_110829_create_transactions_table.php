<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('depositmethod');
            $table->string('referenceno');
            $table->string('transactionphoneno');
            $table->string('memberno');
            $table->integer('amount');
            $table->string('depositorname');
            $table->datetime('transactiontime');
            $table->timestamps();
            $table->foreign('memberno')->references('memberno')->on('users')->onDelete('cascade');
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
