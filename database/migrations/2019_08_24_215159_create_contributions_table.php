<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contributions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('transactionid', 10);
            $table->string('phoneno');
            $table->integer('amount');
            $table->datetime('transactiontime');
            $table->integer('memberno');
            $table->string('modeofpayment');
            $table->string('addedby');
            $table->timestamps();

            $table->foreign('memberno')->references('memberno')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contributions');
    }
}
