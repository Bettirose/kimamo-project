<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    protected $fillable = [
        'depositorname', 'transactionid', 'transactiontime', 'phoneno', 'amount', 'depositmethod', 'memberno'
    ];
}
