<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contribution;
use App\Transactions;
use Illuminate\Support\Facades\Auth;
use App\User;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;
use DB;
use Carbon\Carbon;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $contributions = Transactions::where('memberno', $user->memberno)->get();
                                        

        $AmountCollection = (Transactions::where('memberno', $user->memberno)->get('amount'));
        $sum = collect($AmountCollection)->sum('amount'); 
        $totalcontribution = DB::table('users')
                                ->select('totalcontribution')
                                ->where ('memberno', $user->memberno)
                                ->first();
       // User::where('memberno', $user->memberno)->get('totalcontribution');
       // dd($totalcontribution);
        // $membercontributions = Transactions::join('users','users.memberno','=','transactions.memberno')
        //                                     ->where('memberno')
        //                                     ->select('users.firstname', 'users.lastname', 'users.memberno', 'users.phoneno', 'users.created_at')
        //                                     ->groupBy('users.memberno')
        //                                     ->orderBy('sumAmount','desc')
        //                                     ->get();
        //                                     $transactiondetails = Transactions::join('users','transactions.memberno','=','users.memberno')
        //                                     ->select('transactions.referenceno','transactions.id', 'transactions.transactiontime','transactions.depositmethod','transactions.transactionphoneno','transactions.memberno','transactions.amount','transactions.created_at','users.firstname', 'users.lastname')
        //                                     ->groupby('transactions.id')
        //                                     ->orderby('transactions.created_at', 'desc')
        //                                     ->get();
        $topcontributors = User::join('transactions','users.memberno','=','transactions.memberno')
                        ->select(DB::raw('sum(transactions.amount) AS sumAmount'),'users.firstname', 'users.lastname')
                        ->groupBy('users.memberno')
                        ->orderBy('sumAmount','desc')
                        ->take(5)
                        ->get();
        //dd($contributors);

        //generate charts using consoleTV/charts
    	// $users = User::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))
    	// 			->get();
        // $chart = Charts::database($users, 'bar', 'highcharts')
		// 	      ->title("Monthly new Register Users")
		// 	      ->elementLabel("Total Users")
		// 	      ->dimensions(1000, 500)
		// 	      ->responsive(false)
		// 	      ->groupByMonth(date('Y'), true); 
        $chart_options = [
            'chart_title' => 'Amount by Month',
            'report_type' => 'group_by_date',
            'model' => 'App\Transactions',
            'group_by_field' => 'created_at',
            'group_by_period' => 'month',
            'chart_type' => 'bar',
            'filter_field' => 'created_at',
            'aggregate_function' => 'sum',
            'aggregate_field' => 'amount',
            //'filter_days' => 90, // show only last 30 days
        ];
    
        $chart1 = new LaravelChart($chart_options);
        //dd($chart1);
    
         $chart_options = [
             'chart_title' => 'Amount by Month',
             'report_type' => 'group_by_string',
             'model' => 'App\User',
             'group_by_field' => 'name',
             'chart_type' => 'pie',
             'filter_field' => 'created_at',
             'filter_period' => 'month', // show users only registered this month
         ];
    
         $chart2 = new LaravelChart($chart_options);
    
         $chart_options = [
             'chart_title' => 'Transactions by dates',
             'report_type' => 'group_by_date',
             'model' => 'App\Transactions',
             'group_by_field' => 'transaction_date',
             'group_by_period' => 'day',
             'aggregate_function' => 'sum',
             'aggregate_field' => 'amount',
             'chart_type' => 'line',
         ];
    
         $chart3 = new LaravelChart($chart_options);

         $contributionValues = Transactions::where('memberno', $user->memberno)
                                ->get()
                                ->groupBy(
                                    function ($entry){
                                        return $entry->created_at->format('Y-m');
                                    }
                                )
                                ->map(function ($entries) {
                                    return $entries->{'sum'}('amount');
                                });
        //dd($contributionValues);
        $date = Carbon::now()->startOfMonth()->toDateString();
        //dd($date);
        $revenueMonth = Transactions::where('created_at', '>=', $date) 
                                      ->where ('memberno', $user->memberno)
                                      ->sum('amount');
        return view('home', compact('contributions', 'sum', 'chart1','chart2', 'chart3', 'topcontributors','totalcontribution', 'revenueMonth', 'contributionValues'));
    }
    
}
