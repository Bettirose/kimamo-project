<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contribution;
use App\Transactions;
use Illuminate\Support\Facades\Auth;
use App\User;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;
use DB;
use Carbon\Carbon;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $contributions = Transactions::all();
        $AmountCollection = (User::get('totalcontribution'));
        $sum = collect($AmountCollection)->sum('totalcontribution'); 
        
        // $membercontributions = Transactions::join('users','users.memberno','=','transactions.memberno')
        //                 ->select(DB::raw('sum(transactions.amount) AS sumAmount'),'users.firstname', 'users.lastname', 'users.memberno', 'users.phoneno', 'users.created_at')
        //                 ->groupBy('users.memberno')
        //                 ->orderBy('sumAmount','desc')
        //                 ->get();

        $membercontributions = User::all();
                                    

              
        //   $topcontributors = User::join('transactions','users.memberno','=','transactions.memberno')
        //                   ->select(DB::raw('sum(transactions.amount) AS sumAmount'),'users.firstname', 'users.lastname', 'users.memberno')
        //                   ->groupBy('users.memberno')
        //                   ->orderBy('sumAmount','desc')
        //                   ->take(5)
        //                   ->get();
        //                   $errors = DB::table('errors')
        // ->select('website_id', DB::raw('COUNT(id) as errors'))
        // ->groupBy('website_id')
        // ->orderBy(DB::raw('COUNT(id)'), 'DESC')
        // ->take(10)
        // ->get();

        // $topcontributors = DB::table('users')
        //                     ->select('firstname', 'lastname', 'memberno', 'totalcontribution')
        //                     ->sortBy('totalcontribution')
        //                     ->orderBy('totalcontribution')
        //                     ->groupBy('memberno')
        //                     ->take(5)
        //                     ->get();
        $topcontributors = User::select('totalcontribution','users.firstname', 'users.lastname', 'users.memberno')
                           ->groupBy('users.memberno')
                           ->orderBy('totalcontribution','desc')
                           ->take(5)
                           ->get();
      // dd($topcontributors);

        //generate charts using consoleTV/charts
    	// $users = User::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))
    	// 			->get();
        // $chart = Charts::database($users, 'bar', 'highcharts')
		// 	      ->title("Monthly new Register Users")
		// 	      ->elementLabel("Total Users")
		// 	      ->dimensions(1000, 500)
		// 	      ->responsive(false)
		// 	      ->groupByMonth(date('Y'), true); 
        $chart_options = [
            'chart_title' => 'Amount by Month',
            'report_type' => 'group_by_date',
            'model' => 'App\Transactions',
            'group_by_field' => 'created_at',
            'group_by_period' => 'month',
            'chart_type' => 'bar',
            'filter_field' => 'created_at',
            'aggregate_function' => 'sum',
            'aggregate_field' => 'amount',
            //'filter_days' => 90, // show only last 30 days
        ];
    
        $chart1 = new LaravelChart($chart_options);
        //dd($chart1);
    
         $chart_options = [
             'chart_title' => 'Amount by Month',
             'report_type' => 'group_by_string',
             'model' => 'App\User',
             'group_by_field' => 'name',
             'chart_type' => 'pie',
             'filter_field' => 'created_at',
             'filter_period' => 'month', // show users only registered this month
         ];
    
         $chart2 = new LaravelChart($chart_options);
    
         $chart_options = [
             'chart_title' => 'Transactions by dates',
             'report_type' => 'group_by_date',
             'model' => 'App\Transactions',
             'group_by_field' => 'transaction_date',
             'group_by_period' => 'day',
             'aggregate_function' => 'sum',
             'aggregate_field' => 'amount',
             'chart_type' => 'line',
         ];
    
         $chart3 = new LaravelChart($chart_options);

         $contributionValues = Transactions::all()
                                ->groupBy(
                                    function ($entry){
                                        return $entry->created_at->format('Y-m');
                                    }
                                )
                                ->map(function ($entries) {
                                    return $entries->{'sum'}('amount');
                                });
        //dd($contributionValues);
        $date = Carbon::now()->startOfMonth()->toDateString();
        //dd($date);
        $revenueMonth = Transactions::where(
            'created_at', '>=', $date
        )->sum('amount');

      // dd ($membercontributions);
        return view('admin', compact('contributions', 'sum', 'chart1','chart2', 'chart3', 'membercontributions','revenueMonth','topcontributors', 'contributionValues'));
    }
}
