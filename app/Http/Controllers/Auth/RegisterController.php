<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/members';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
     {
        $this->middleware('auth');
     }

    public function showRegistrationForm()
    {
        return view('/auth/register');

    }
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        // $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'memberno' => ['required', 'integer', 'unique:users'],
            // 'phoneno' => ['string', 'max:11'],
            // 'email' => ['string', 'email', 'max:255', 'unique:users'],
            // 'nationalid' => ['unique:users', 'string', 'max:10'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'roleid' => ['boolean'],
            'totalcontribution' => ['integer'],

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'email' => $data['email'],
            'memberno' => $data['memberno'],
            'phoneno' => $data['phoneno'],
            'password' => Hash::make($data['password']),
            'roleid' => $data['roleid'],
            'nationalid' => $data['nationalid'],
            'totalcontribution' => $data['totalcontribution'],
        ]);
    }
    
}
