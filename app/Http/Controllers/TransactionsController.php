<?php

namespace App\Http\Controllers;

use App\Transactions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use DB;

class TransactionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $transactions = Transactions::all();
        $users = User::all();
        $transactiondetails = Transactions::join('users','transactions.memberno','=','users.memberno')
                                    ->select('transactions.referenceno','transactions.id', 'transactions.transactiontime','transactions.depositmethod','transactions.transactionphoneno','transactions.memberno','transactions.amount','transactions.created_at','users.firstname', 'users.lastname')
                                    ->groupby('transactions.id')
                                    ->orderby('transactions.created_at', 'desc')
                                    ->get();
       
        return view('transactions.index', compact('transactiondetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('transactions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //store new transactions
        $transaction = new Transactions();
        $user = new User();
       
        $transaction->depositorname = request('name');
        $transaction->transactiontime = request('transactiontime');
        $transaction->referenceno = request('transactionid');
        $transaction->transactionphoneno = request('phoneno');
        $transaction->memberno = request('memberno');
        $transaction->amount = request('amount');
        $transaction->depositmethod = request('modeofpayment');

        $transaction->save();
        $contribution = request('amount');
        $memberno = request('memberno');
        $totalcontribution = DB::table('users')->where('memberno', $memberno)->first();
        $newtotalcontribution = (int)$totalcontribution->totalcontribution + (int)$contribution;
        $affected = DB::table('users')
            ->where('memberno', $memberno)
            ->update(['totalcontribution' => $newtotalcontribution]);
        //dd($newtotalcontribution);
        return redirect('/transactions')
                        ->with('success','Transaction created successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function show(Transactions $Transaction)
    {
        //show the index which has all transactions
        
        //abort_if($transaction->memberno !== $user->memberno, 403);
        return view ('transactions.index', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function edit(Transactions $transaction)
    {
        //edit a transaction
       // $transaction = Transactions::findOrFail($id);
        return view('transactions.edit', compact('transaction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transactions $transaction)
    {
        //update a transaction
        $transaction = new Transactions();

       
        $transaction->depositorname = request('name');
        $transaction->referenceno = request('transactionid');
        $transaction->transactionphoneno = request('phoneno');
        $transaction->memberno = request('memberno');
        $transaction->amount = request('amount');
        $transaction->depositmethod = request('modeofpayment');

        $transaction->save();

        return redirect('/transactions')->with('success', "Product updated successfully");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transactions  $transactions
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transactions $transaction)
    {
        //
        $transaction->delete();
        // Transactions::findOrFail($id)->delete();
        return redirect('/transactions')->with('success', "Product deleted sucessfully");
    }
}
