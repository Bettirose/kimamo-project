<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contribution;
use App\User;
use Illuminate\Support\Facades\Auth;
use LaravelDaily\LaravelCharts\Classes\LaravelChart;
use Carbon\Carbon;
use DB;
class ContributionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $user = Auth::user();
          $contributions = Contribution::select('contributions.*')
                            ->orderBy('contributions.created_at','desc')
                            ->get();
        //  $AmountCollection = (Contribution::where('memberno', $user->memberno)->get('amount'));
        //  $sum = collect($AmountCollection)->sum('amount'); 
        $totalMemberContributions = Contribution::select('contributions.amount')->sum('amount');
        //dd($totalMemberContributions);
        
        $topcontributors = User::join('contributions','users.memberno','=','contributions.memberno')
                        ->select(DB::raw('sum(contributions.amount) AS sumAmount'),'users.firstname', 'users.lastname')
                        ->groupBy('users.memberno')
                        ->orderBy('sumAmount','desc')
                        ->take(3)
                        ->get();
        //dd($contributors);

        
        $chart_options = [
            'chart_title' => 'Amount by Month',
            'report_type' => 'group_by_date',
            'model' => 'App\Contribution',
            'group_by_field' => 'created_at',
            'group_by_period' => 'month',
            'chart_type' => 'bar',
            'filter_field' => 'created_at',
            'aggregate_function' => 'sum',
            'aggregate_field' => 'amount',
            //'filter_days' => 90, // show only last 30 days
        ];
    
        $chart1 = new LaravelChart($chart_options);
        //dd($chart1);

        $date = Carbon::now()->startOfMonth()->toDateString();
        //dd($date);
        $revenueMonth = Contribution::where(
            'created_at', '>=', $date
        )->sum('amount');
        return view('contributions', compact('contributions', 'totalMemberContributions', 'chart1','chart2', 'chart3', 'topcontributors', 'revenueMonth'));
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contributions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'name' => 'required',
            'transactionid' => ['required', 'max:15'],
            'phoneno' => 'required',
            'amount' => 'required',
            'transactiontime' => 'required',
            'memberno' => 'required',
            'modeofpayment' => 'required',
            'addedby' => 'max:100'
        ]);
        Contribution::create($attributes);
        
        return redirect('/admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort_if($contribution->memberno !== $user->memberno, 403);
        return view ('home', compact('contribution'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Contribution $contribution)
    {
        return view('contributions.edit', compact('contribution'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Contribution $contribution)
    {
        $user = Auth::user();
        $contribution->name = request('name');
        $contribution->transactionid = request('transactionid');
        $contribution->phoneno = request('phoneno');
        $contribution->memberno = request('memberno');
        $contribution->transactiontime = request('transactiontime');
        $contribution->amount = request('amount');
        $contribution->modeofpayment = request('modeofpayment');
        $contribution->addedby = $user->email;

        $contribution->save();
        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contribution $contribution)
    {
        $contribution->delete();
        return redirect('/home');
    }
}
