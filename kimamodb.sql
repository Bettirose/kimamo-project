-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 26, 2020 at 12:09 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kimamodb`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `roleid` int(11) NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `memberno` int(11) NOT NULL,
  `nationalid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phoneno` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `totalcontribution` int(11) NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_memberno_unique` (`memberno`)
) ENGINE=MyISAM AUTO_INCREMENT=123 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `roleid`, `firstname`, `lastname`, `memberno`, `nationalid`, `phoneno`, `totalcontribution`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'Kimamo', 1000, '', '', 0, '', '2020-04-22 21:00:00', '$2y$10$NxxhNB4epelWiOO/s3NpIODEJOY/xMDtLNkIySoU09/gh887ggbbW', NULL, '2020-04-22 21:00:00', '2020-04-22 21:00:00'),
(2, 0, 'Joseph', 'Mwangi Ben', 2, 'NA', 'NA', 8000, 'NA', NULL, '$2y$10$FHeHX1B8olTGmlZ4YGYnmOfRrHyVv/ZO0HCFbZWS7Mw/n54k8HT5e', NULL, '2020-04-23 12:41:42', '2020-04-23 12:41:42'),
(3, 0, 'Simon', 'Gathaiya Kamau', 1, 'NA', 'NA', 8000, 'sgathy85@gmail.com', NULL, '$2y$10$bIqe0kXD/JcxSKAJ0r36Geuy/S1Gz2b9vC8MsZmXaQeU/uEuYoQj2', NULL, '2020-04-23 12:42:59', '2020-04-23 12:42:59'),
(4, 0, 'Julius', 'Ndugi Kirubi', 3, 'NA', 'NA', 8000, 'NA', NULL, '$2y$10$xXiWiG0jJaW7kkjyb.6hbuH4RCDZFkuz1j.tEeqa7LftKr7vU4m0C', NULL, '2020-04-23 12:44:40', '2020-04-23 12:44:40'),
(5, 0, 'Robert', 'Kangethe Mioro', 4, 'NA', 'NA', 4500, 'NA', NULL, '$2y$10$WWUIdVfanCB4sb0hbJOUzudw.8Lcg5K8DhKmvdRRzmSaCiEO.bMRa', NULL, '2020-04-23 12:45:41', '2020-04-23 12:45:41'),
(6, 0, 'Jason', 'Waiganjo', 5, 'NA', 'NA', 12000, 'NA', NULL, '$2y$10$7R3FJqp9GwukqycY9CyFW.GV1Gf2eFnW98q7sjUaEURt/Ts/7xTIG', NULL, '2020-04-23 12:46:29', '2020-04-23 12:46:29'),
(7, 0, 'Salome', 'Lilian Wanjira', 6, 'NA', 'NA', 10000, 'NA', NULL, '$2y$10$LAuVwd9NfeNyYTFNyTW1hu7Afh10AKmkdHOqDbxDpdeC0yx4Ct5T6', NULL, '2020-04-23 12:47:09', '2020-04-23 12:47:09'),
(8, 0, 'Harrison', 'Githongo Macharia', 7, 'NA', 'NA', 7000, 'NA', NULL, '$2y$10$3rimMWfxdgBj17KIDbYNM.LnnRw.D8Jxehmw5aM.mlbQ0WcjrqIva', NULL, '2020-04-23 12:47:54', '2020-04-23 12:47:54'),
(9, 0, 'Eliud', 'Kimani James', 8, 'NA', 'NA', 500, 'NA', NULL, '$2y$10$BRrUrOGj4msuJgSm6uCxEOFQER75x5E4CRYRLpsMq4B3uF4vfzgsC', NULL, '2020-04-23 12:49:44', '2020-04-23 12:49:44'),
(10, 0, 'Joseph', 'Njioka Irungu', 9, 'NA', 'NA', 8500, 'NA', NULL, '$2y$10$KL12e5W7VlEiYVIY9Ax3p.Q78/SO/01gOZBhGvN7jzyXE5bl.6BSy', NULL, '2020-04-23 12:50:45', '2020-04-23 12:50:45'),
(11, 0, 'David', 'Muchina Wanjohi', 10, 'NA', 'NA', 4000, 'NA', NULL, '$2y$10$c7TDwmjsxWfHoYvbWqTTq.BoawPcNVHIKpTdsvlxpyrc8zR2Iiham', NULL, '2020-04-23 12:55:43', '2020-04-23 12:55:43'),
(12, 0, 'William', 'Mwangi Kimwe', 11, 'NA', 'NA', 3000, 'NA', NULL, '$2y$10$5mecmIaPNS5GKO60Weeyw.gDyO93kITBrsoT1xrWzDvPlKaeMRmly', NULL, '2020-04-23 12:56:22', '2020-04-23 12:56:22'),
(13, 0, 'Samuel', 'P. Mwenda Kamau', 12, 'NA', 'NA', 9500, 'NA', NULL, '$2y$10$Ak44oo2O1CUKrJIqdtFdGOUm1Tb3qwMSJw35c3nMkyiwe8IP/jxkS', NULL, '2020-04-23 12:57:11', '2020-04-23 12:57:11'),
(14, 0, 'Humphrey', 'Warungu Gikonyo', 13, 'NA', 'NA', 7500, 'NA', NULL, '$2y$10$cEsiNt1R1Yu.6WIWUJ8RL.OGq3noSSqZIQY1oFKotn3cM5.GVTble', NULL, '2020-04-23 12:58:07', '2020-04-23 12:58:07'),
(15, 0, 'Esther', 'Wanjiku Kibunja', 14, 'NA', 'NA', 11000, 'NA', NULL, '$2y$10$Urt/i7sf0clgRY7oEniro./edQtqyuA94kw6a7rF/hxBB3U77nEHG', NULL, '2020-04-23 12:59:02', '2020-04-23 12:59:02'),
(16, 0, 'Robert', 'Maina Karanja', 15, 'NA', 'NA', 3500, 'NA', NULL, '$2y$10$l0Xe393IKO0kvCKfXJzB9OfSI4rAVTk6vrTdmXq4.iJjSrmfzQJk2', NULL, '2020-04-23 13:00:00', '2020-04-23 13:00:00'),
(17, 0, 'Charles', 'Muna Kimani', 16, 'NA', 'NA', 1000, 'NA', NULL, '$2y$10$J1j9kR1AKR6nOTcF2VvWeOQAlrcCumXew4FdFvvd5HVtE5tuf.IGS', NULL, '2020-04-23 13:00:43', '2020-04-23 13:00:43'),
(18, 0, 'Joseph', 'Kang\'ethe Hinga', 17, 'NA', 'NA', 5500, 'NA', NULL, '$2y$10$LrG.Gnl3POBw9sUejbddPumvjTbmSudaOAqFir6nROMAx2153uSk2', NULL, '2020-04-23 13:01:36', '2020-04-23 13:01:36'),
(19, 0, 'Esther', 'Wanjiku Njuguna', 18, 'NA', 'NA', 7000, 'NA', NULL, '$2y$10$BCyGu/AEgcDgrhM1.bWjbeeZNiDMf64WEv7KMyLUBF66gEsP36eSS', NULL, '2020-04-23 13:02:49', '2020-04-23 13:02:49'),
(20, 0, 'Peter', 'Kamau Njioka', 19, 'NA', 'NA', 11500, 'NA', NULL, '$2y$10$OKIIv.H7yjriRCKSjtvsX.7nY6nncoT0mbfd.PZg2aoIKRLod0TfW', NULL, '2020-04-23 13:03:50', '2020-04-23 13:03:50'),
(21, 0, 'Alex', 'Njoroge Mwaura', 20, 'NA', 'NA', 0, 'NA', NULL, '$2y$10$gkEaYQ82xerSqkvpDtHwLO8MosjdZcjM7PL9WamIR5s2SzoRFMeY6', NULL, '2020-04-23 13:04:45', '2020-04-23 13:04:45'),
(22, 0, 'Grace', 'Wangui Muchoki', 21, 'NA', 'NA', 10500, 'NA', NULL, '$2y$10$TzbeuWb6Uv8LeZnEcn6E7uwmNGluQjdZkbeVsE2dkPQJFiPbwtuLy', NULL, '2020-04-23 13:05:31', '2020-04-23 13:05:31'),
(23, 0, 'John', 'Elias Thuo', 22, 'NA', 'NA', 4500, 'NA', NULL, '$2y$10$ZotqVGaDLkHfRFu8i5QoZevoQeR9vQtpfzTclAoBKk/Gx5hI0ZxwO', NULL, '2020-04-23 13:06:32', '2020-04-23 13:06:32'),
(24, 0, 'Bonface', 'Gikandi', 23, 'NA', 'NA', 2000, 'NA', NULL, '$2y$10$f9ex7sps8HsTHuGzKuJJMOD06CFLQUBRNYo2ceu.pnohkLmHbmP6a', NULL, '2020-04-23 13:07:28', '2020-04-23 13:07:28'),
(25, 0, 'Anthony', 'Kinuthia Mwicigi', 24, 'NA', 'NA', 9000, 'NA', NULL, '$2y$10$5059CR7UkayasINJPSS9i.Ygk/XSKZi2cZlfBKhTU7idTlTv6l31O', NULL, '2020-04-23 13:08:15', '2020-04-23 13:08:15'),
(26, 0, 'John', 'Ngandu Githumbi', 25, 'NA', 'NA', 8000, 'NA', NULL, '$2y$10$NXDfq.HyzU2bovX6iXytoOeOlerMIPyXJT.bBNHagw5irCauUjmGa', NULL, '2020-04-23 13:09:24', '2020-04-23 13:09:24'),
(27, 0, 'Felix', 'Kimotho', 26, 'NA', 'NA', 11500, 'NA', NULL, '$2y$10$GWlsNA.C9t4yYBT.cS7dzeU0PVpMnRmvSMRogt0ey9nlC5/arTzv6', NULL, '2020-04-23 13:11:03', '2020-04-23 13:11:03'),
(28, 0, 'Michael', 'Irungu Chege', 27, 'NA', 'NA', 9500, 'NA', NULL, '$2y$10$yshcGfEhK88fCs7BI.uEXOZo6YAivsfFx4JHlmsL13Cwooz.IB1mK', NULL, '2020-04-23 13:11:53', '2020-04-23 13:11:53'),
(29, 0, 'Robinson', 'Stephen Gakuru', 28, 'NA', 'NA', 12000, 'NA', NULL, '$2y$10$OeUGMx667ZwOsiad1EE34uXn0UuPeQ5zsAmUs0pH9fXrS6iiau6Jy', NULL, '2020-04-23 13:12:44', '2020-04-23 13:12:44'),
(30, 0, 'Alexander', 'N. Kihara', 29, 'NA', 'NA', 1500, 'NA', NULL, '$2y$10$3EnuqVC.x5/AV20fuy2RtuXIBaoe5t8.xVOrNl2ln7YpFJc1lPJjq', NULL, '2020-04-23 13:16:02', '2020-04-23 13:16:02'),
(31, 0, 'Joseph', 'Ng\'ang\'a Irungu', 30, 'NA', 'NA', 11000, 'NA', NULL, '$2y$10$3WGXamLMARdcdg0mR6kNSOvuK3/U/JHcD4L6wjW30s4RwhjdvWhQe', NULL, '2020-04-23 13:16:55', '2020-04-23 13:16:55'),
(32, 0, 'Timothy', 'Muchina Nduati', 31, 'NA', 'NA', 10500, 'NA', NULL, '$2y$10$mO0tw4UBAMTZZBaoxqCUPOBKoTfbWPzlwKxB4CHEkQ4pW1R8FFC/2', NULL, '2020-04-23 13:18:02', '2020-04-23 13:18:02'),
(33, 0, 'Cyrus', 'Macharia Mwangi', 32, 'NA', 'NA', 6500, 'NA', NULL, '$2y$10$nK1p4ii2EsczfDcZyOnViuENnS0ENky/cdSfpeHh0suq.Is1Nsk9i', NULL, '2020-04-23 13:18:43', '2020-04-23 13:18:43'),
(34, 0, 'Kenneth', 'Ruua Migwi', 33, 'NA', 'NA', 10000, 'NA', NULL, '$2y$10$i.JWVYJKIIsX.5RyksZCf.Rkq4b7esvy.NYAjdbfQrkz7IX1P2ify', NULL, '2020-04-23 13:20:05', '2020-04-23 13:20:05'),
(35, 0, 'Stanley', 'Mburu Wanyoike', 34, 'NA', 'NA', 6000, 'NA', NULL, '$2y$10$nVrom.jMA36JczIg9LEcgOxvz58GJwUo/2sPB3ZmtjJ4HWs4n5D.u', NULL, '2020-04-23 13:22:00', '2020-04-23 13:22:00'),
(36, 0, 'David', 'Wokabi Mwangi', 35, 'NA', 'NA', 6000, 'NA', NULL, '$2y$10$.CmiWjJV80mJ7h2vD/TqXOhZOtpP88vYOeAXGnozHkHQVAh320Kqu', NULL, '2020-04-23 14:25:01', '2020-04-23 14:25:01'),
(37, 0, 'Duncan', 'Githae Kirugumi', 36, 'NA', 'NA', 12000, 'NA', NULL, '$2y$10$kq2rutY7beuqocJPsbfn1uiYWB34s.FSEFgb8Cshs1.HJa8aKe7hW', NULL, '2020-04-23 14:26:07', '2020-04-23 14:26:07'),
(38, 0, 'Paul', 'Maina Thuku', 37, 'NA', 'NA', 11000, 'NA', NULL, '$2y$10$y5BSaBNz3xbIEzdFZ/Sf2OEicMIawmxvXZKZNQIgdEI.At8Tkei/u', NULL, '2020-04-23 14:26:55', '2020-04-23 14:26:55'),
(39, 0, 'Jane', 'Wanjira Thuo', 38, 'NA', 'NA', 0, 'NA', NULL, '$2y$10$rRa5F95bWJ6XvrrPY9Z7fuupllv8NzMDmf.I7OsXMq6VvHhpUFGm6', NULL, '2020-04-23 14:27:32', '2020-04-23 14:27:32'),
(40, 0, 'Jane', 'Mwikali Maina', 39, 'NA', 'NA', 7500, 'NA', NULL, '$2y$10$hdgOoqZZeFNGKHag7peh.O0YaDkmNBsBulKnj6SueTEVsloJ5MMRy', NULL, '2020-04-23 14:28:07', '2020-04-23 14:28:07'),
(41, 0, 'Simon', 'Wanjau Mulili', 40, 'NA', 'NA', 9500, 'NA', NULL, '$2y$10$tPgZMyOjF6oOCJ5tqwCiEOfGrBJrr1td4oVj6r6/4J1C20bdZPSjC', NULL, '2020-04-23 14:28:49', '2020-04-23 14:28:49'),
(42, 0, 'Elizabeth', 'Wanja Kariuki', 41, 'NA', 'NA', 4500, 'NA', NULL, '$2y$10$6ACukvUOuQ5SECyXNveYkuwxOjcDDYMp5h14jJrE9V7zu7JPV.HCS', NULL, '2020-04-23 14:29:27', '2020-04-23 14:29:27'),
(43, 0, 'Esther', 'Muthoni Mwangi', 42, 'NA', 'NA', 8000, 'NA', NULL, '$2y$10$79b3z70G5luNe6p5UX02XOwWxwQUYC4i7gnbuW3Yh8TKMug4D5mom', NULL, '2020-04-23 14:30:04', '2020-04-23 14:30:04'),
(44, 0, 'Angeline', 'Wanjiku Kuria', 43, 'NA', 'NA', 12000, 'NA', NULL, '$2y$10$XFXelTCFs8PDxutklIwm1uSthz2T/PhJpu1TREr4MTQTrzYEslAT.', NULL, '2020-04-23 14:30:55', '2020-04-23 14:30:55'),
(45, 0, 'Patrick', 'Kamau Mwangi', 44, 'NA', 'NA', 7000, 'NA', NULL, '$2y$10$EV101Cq4k7K.ATuKH21l8epQHeZUyXA0BHMoaOtyXsaqgvKa4V6e.', NULL, '2020-04-23 14:39:58', '2020-04-23 14:39:58'),
(46, 0, 'Elijah', 'Muriithi Nderitu', 45, 'NA', 'NA', 3000, 'NA', NULL, '$2y$10$ZQoBdVDz0UHsNUabNvnGauT0d9FsUX6.xy/beD1SB58rMnOSeJ8bG', NULL, '2020-04-23 14:40:50', '2020-04-23 14:40:50'),
(47, 0, 'David', 'Kungu Gatimu', 46, 'NA', 'NA', 7000, 'NA', NULL, '$2y$10$Ad/8mz3hXXAAnOxpalbKgem.Ud6l6dKDWZJ8CNQRhW3AuBNx0DtpS', NULL, '2020-04-23 14:41:29', '2020-04-23 14:41:29'),
(48, 0, 'Patrick', 'Mwaniki Warima', 47, 'NA', 'NA', 8000, 'NA', NULL, '$2y$10$9ZBRU8D51b9lUEQEpYIKzu8t7qiVOA4vxPkv.IM32v6ieIEiIhatK', NULL, '2020-04-23 14:42:14', '2020-04-23 14:42:14'),
(49, 0, 'Willian', 'Wanjama Kuria', 48, 'NA', 'NA', 0, 'NA', NULL, '$2y$10$vgeg3MchoyfYRD0BP.AwKeEVBj3cLx94ZvwcsHySJzRc4mvNRscI2', NULL, '2020-04-23 14:43:04', '2020-04-23 14:43:04'),
(50, 0, 'Jackson', 'Melchisedeck Waweru', 49, 'NA', 'NA', 7000, 'NA', NULL, '$2y$10$tWu/ecLpFDg7Y/a8Ykt0yu9V/a8pQZRygTlMQ26BcVlQVif1.Ipfy', NULL, '2020-04-23 14:43:46', '2020-04-23 14:43:46'),
(51, 0, 'Nelson', 'Mutugi Githendu', 50, 'NA', 'NA', 0, 'NA', NULL, '$2y$10$nPjAvVJehDBIOHGljkkru.zJScDbAXwQ2J.5ahx7whlBOFPljx5UC', NULL, '2020-04-23 14:44:20', '2020-04-23 14:44:20'),
(52, 0, 'Peter', 'Macharia Kaguyu', 51, 'NA', 'NA', 3500, 'NA', NULL, '$2y$10$1PwRVCmewW1XHo3f1T1PdueK/mwbgGMmjfydfbIu9gvOMpDJHgv2y', NULL, '2020-04-23 14:45:10', '2020-04-23 14:45:10'),
(53, 0, 'Suleiman', 'Mwangi Hassan', 52, 'NA', 'NA', 12000, 'NA', NULL, '$2y$10$5voNYbfgBTGgLRfzuktNLuWGh8fn0gFdDZOIfpvpUo9CSZaVK/fwa', NULL, '2020-04-23 14:45:46', '2020-04-23 14:45:46'),
(54, 0, 'Anderson', 'Maina Gachagua', 53, 'NA', 'NA', 6000, 'NA', NULL, '$2y$10$zywXcSaLAS9Fh36mvuc97u2aJq9EMu8KAbqaOGIU3lY6/1VRfklhu', NULL, '2020-04-23 14:46:27', '2020-04-23 14:46:27'),
(55, 0, 'Sarah', 'Ngunju Njongo', 54, 'NA', 'NA', 5500, 'NA', NULL, '$2y$10$eqrMVedkibpOuL7EWgNoDubAs4F5sAvfGWxwpmh3VVS1uq6LoXahS', NULL, '2020-04-23 14:47:24', '2020-04-23 14:47:24'),
(56, 0, 'Bernard', 'King\'ori Nyakinyua', 55, 'NA', 'NA', 8000, 'NA', NULL, '$2y$10$WV/0pUP7XhMJbWM7sCDg4eN/fCEZUT6b2kDf2.Q9CI0A7aydSo3C2', NULL, '2020-04-23 14:48:52', '2020-04-23 14:48:52'),
(57, 0, 'Danson', 'Ngure Thuo', 56, 'NA', 'NA', 7500, 'NA', NULL, '$2y$10$FlMOexovLWfB93iDBu95vO0MviT/fKOB7T6VdSsrHCg/QqMEhKVcS', NULL, '2020-04-23 14:50:23', '2020-04-23 14:50:23'),
(58, 0, 'Jane', 'Wanjiku Mwaniki', 57, 'NA', 'NA', 8000, 'NA', NULL, '$2y$10$WOSrxODz0h.MO7mx.rUdCO4uXvZ4u4P4lyPRxQ3EZUf/b7YrRnrwu', NULL, '2020-04-23 14:51:07', '2020-04-23 14:51:07'),
(59, 0, 'Pauline', 'Wambui Maina', 58, 'NA', 'NA', 6500, 'NA', NULL, '$2y$10$.oKU2CeF5Pxk0Lb9Vg.w9e2hy.EXrTNnXjytaBR0WJRNmhDLU3VMm', NULL, '2020-04-23 14:54:28', '2020-04-23 14:54:28'),
(60, 0, 'Ezra', 'Kagimbi Kamau', 59, 'NA', 'NA', 6000, 'NA', NULL, '$2y$10$cArJs357vt3/ElWavfcCDOsfFu0yLcL9a1t24jrkwGMLJRFrSiMsC', NULL, '2020-04-23 14:55:08', '2020-04-23 14:55:08'),
(61, 0, 'Margaret', 'Njeri Gakuo', 60, 'NA', 'NA', 7500, 'NA', NULL, '$2y$10$IGph03CcmLkpyEjpkE6dxePibhKn05IaO3IJPDtK7JH3dZ2ZNd6FK', NULL, '2020-04-23 14:55:46', '2020-04-23 14:55:46'),
(62, 0, 'Julia', 'Wambui Mbao', 61, 'NA', 'NA', 3500, 'NA', NULL, '$2y$10$Q4PVJyXc0ZVtNfkuLNhZHelNdwwO0iA1x5pWFoHXZg2vvD7mJ6lxG', NULL, '2020-04-23 14:56:20', '2020-04-23 14:56:20'),
(63, 0, 'Peter', 'Mugo Maina', 62, 'NA', 'NA', 6000, 'NA', NULL, '$2y$10$wqeYAaPlatfUZbmDCUc/X.6sJI28ZPnq97b2jv4YLvN4r5b3ajkqu', NULL, '2020-04-23 14:57:12', '2020-04-23 14:57:12'),
(64, 0, 'Joyce', 'Karungari Wachira', 63, 'NA', 'NA', 5500, 'NA', NULL, '$2y$10$xidHRMr8t6m5kIjYM.MryevtRz6tnUVJNbNmLSoMhrg5xqVTirtgG', NULL, '2020-04-23 14:59:11', '2020-04-23 14:59:11'),
(65, 0, 'Judy', 'Wanjiru Chege', 64, 'NA', 'NA', 5500, 'NA', NULL, '$2y$10$slPuFTOUN0y/Hwk8Fsf8vuGarxtr/65NKwzHrqk1cuX7SJt8JHOh2', NULL, '2020-04-23 14:59:49', '2020-04-23 14:59:49'),
(66, 0, 'Regina', 'Wacuka Mau', 65, 'NA', 'NA', 11000, 'NA', NULL, '$2y$10$22FMlbQJtUKUHGcH9/I1MOpoZU9jmorYv2iEStBCmdsgvANHBKYpO', NULL, '2020-04-23 15:00:25', '2020-04-23 15:00:25'),
(67, 0, 'Anthony', 'Wahome Gitonga', 66, 'NA', 'NA', 6000, 'NA', NULL, '$2y$10$4nqvNi9agYeFnGXgqIw63.BH1JVrauyvmTv54Ma5NvNnDs9FsLcMW', NULL, '2020-04-23 15:01:11', '2020-04-23 15:01:11'),
(68, 0, 'Martin', 'Kamau Mwangi', 67, 'NA', 'NA', 10500, 'NA', NULL, '$2y$10$1CtEDwbFIuAJLxBj0pYN0.FHbrEA1NJrn.TAaQTR1TGN8VDO3lzwi', NULL, '2020-04-23 15:03:06', '2020-04-23 15:03:06'),
(69, 0, 'Justus', 'Kimani Wangire', 68, 'NA', 'NA', 3000, 'NA', NULL, '$2y$10$2XqeiPDRW7UQQQ58g7aeku.mPXV0DpA.xrV.dV9L7xJrq1QKP7vkW', NULL, '2020-04-23 15:03:37', '2020-04-23 15:03:37'),
(70, 0, 'Samuel', 'Gathira Kirubi', 69, 'NA', 'NA', 5500, 'NA', NULL, '$2y$10$PBqZy59vbVh.LFTY610wwuQgDV.OuMYRJ6zeTaZZJVv16zXF/Hzw.', NULL, '2020-04-23 15:04:11', '2020-04-23 15:04:11'),
(71, 0, 'David', 'Ng\'ang\'a Ngari', 70, 'NA', 'NA', 9500, 'NA', NULL, '$2y$10$p1O4Q9sxkMuOEs/8MXI6Yu.IGPXUe/HrmdwETxA1STQBB4fGQBYzW', NULL, '2020-04-23 15:04:41', '2020-04-23 15:04:41'),
(72, 0, 'Anthony', 'Mwangi Mutiria', 71, 'NA', 'NA', 6500, 'NA', NULL, '$2y$10$p2A3JdbK7EmTztZ3GZvmseJxghJNV7z5ibIASSVuqTo7sPQZN0NCa', NULL, '2020-04-23 15:07:04', '2020-04-23 15:07:04'),
(73, 0, 'Patrick', 'Kihiu Mwaniki', 72, 'NA', 'NA', 7000, 'NA', NULL, '$2y$10$wFgE4ek0XBVuR724YSyWs.N5FMF5pNRG3eDRN.JF4.XkPx767yL3G', NULL, '2020-04-23 15:08:09', '2020-04-23 15:08:09'),
(74, 0, 'Samuel', 'Kamemia Irungu', 73, 'NA', 'NA', 5500, 'NA', NULL, '$2y$10$IWygJaqo9DnAh.TFo54mquX24s8TVyEsV.UT3GMToJEoLpqhGtGxe', NULL, '2020-04-23 15:08:45', '2020-04-23 15:08:45'),
(75, 0, 'Cyprian', 'Kiprotich Wamalwa', 74, 'NA', 'NA', 7000, 'NA', NULL, '$2y$10$5nqHB5VwOJlDFmuSMWCBWu2Mz/N1A039fNhR1L/1ZtXEBcFCbzadG', NULL, '2020-04-23 15:09:22', '2020-04-23 15:09:22'),
(76, 0, 'Eric', 'Maina Muthoni', 75, 'NA', 'NA', 6500, 'NA', NULL, '$2y$10$67fgCHZn9N.gCUO1p5aepea53N1aV8o6vid5jCKeWYGXSJGU/Nmha', NULL, '2020-04-23 15:09:50', '2020-04-23 15:09:50'),
(77, 0, 'Samuel', 'Nyabiagoro Okero', 76, 'NA', 'NA', 8500, 'NA', NULL, '$2y$10$3bhYkAIGktI/qX.eIQ3or.Tk0A8Tb24zlCiSS0eb4BhB5/SoDjyJe', NULL, '2020-04-23 15:10:42', '2020-04-23 15:10:42'),
(78, 0, 'John', 'Kamau Nyaguthii', 77, 'NA', 'NA', 8500, 'NA', NULL, '$2y$10$LquVJVsAXQjBIPpoG6fjqOkRMiHRHq2.1tjnT1FTvb/oQWDH5/pBm', NULL, '2020-04-23 15:11:12', '2020-04-23 15:11:12'),
(79, 0, 'Michael', 'Muchoki Njogu', 78, 'NA', 'NA', 1000, 'NA', NULL, '$2y$10$PgKB0pLZtvPIdWtI1mWtG.ejgQS9C/qP9PGfssQFwkFAb2C5DH506', NULL, '2020-04-23 15:11:55', '2020-04-23 15:11:55'),
(80, 0, 'Francis', 'Muchoki Njogu', 79, 'NA', 'NA', 9500, 'NA', NULL, '$2y$10$qkLTjXrorEtGcz310/rMLO2zURpCRYs/3leotIF.IRbIzjlWtDpWW', NULL, '2020-04-23 15:12:39', '2020-04-23 15:12:39'),
(81, 0, 'Mary', 'Wairimu Mutune', 80, 'NA', 'NA', 6500, 'NA', NULL, '$2y$10$3D.3Vq0Jvvg2V/vXnSPKqelzzVn5QGTL5QFB9CJzUlONrRHO/HZBy', NULL, '2020-04-23 15:13:11', '2020-04-23 15:13:11'),
(82, 0, 'Kennedy', 'Mwangi Kamau', 81, 'NA', 'NA', 3000, 'NA', NULL, '$2y$10$Q0foUaIfddebdtgptMD.r.eazHiiNtTREu9.nr/7jMMjgYFoFeFOm', NULL, '2020-04-23 15:13:39', '2020-04-23 15:13:39'),
(83, 0, 'Denis', 'Kirubi Muthuma', 82, 'NA', 'NA', 4500, 'NA', NULL, '$2y$10$rl24E211N8tDW.wDXg8i1uaDEPgpUTWuiW3amGmhiRff5XOZI6mx.', NULL, '2020-04-23 15:14:10', '2020-04-23 15:14:10'),
(84, 0, 'Kenneth', 'Waweru', 83, 'NA', 'NA', 2500, 'NA', NULL, '$2y$10$Lus9eLJE4sCav83/qJGsKuu86uGM5umbfgKdWD1u3NMm3ZC73vHem', NULL, '2020-04-23 15:14:53', '2020-04-23 15:14:53'),
(85, 0, 'Joseph', 'Njoroge Wanjiru', 84, 'NA', 'NA', 10000, 'NA', NULL, '$2y$10$xbSM8gtTyrVUBIPbblx.p.k5KCmdN5zvqBEP8DtISrVM7Uk1I/f7m', NULL, '2020-04-23 15:15:27', '2020-04-23 15:15:27'),
(86, 0, 'Simon', 'Irungu Kiaro', 85, 'NA', 'NA', 5500, 'NA', NULL, '$2y$10$QJxfOovRIUFSoWVG//y07uNYY2MRbVit.jvv6cvjfZFPTQGWVDXo2', NULL, '2020-04-23 15:16:27', '2020-04-23 15:16:27'),
(87, 0, 'Geofrey', 'Kimondo Gitonga', 86, 'NA', 'NA', 8000, 'NA', NULL, '$2y$10$bEXQdoKzAgZ3Rjr/3dyQQuPeJPMIhrOp.KOTVuak6q3m7Vj8B/Sva', NULL, '2020-04-23 15:16:59', '2020-04-23 15:16:59'),
(88, 0, 'William', 'King\'ori Mwangi', 87, 'NA', 'NA', 2500, 'NA', NULL, '$2y$10$Y22hkQKRDqpuwB/nR0bsF.J8ECnJvbABEcdrXdS3oL850gfB6JZNS', NULL, '2020-04-23 15:17:41', '2020-04-23 15:17:41'),
(89, 0, 'Jane', 'Ruguru Ngugi', 88, 'NA', 'NA', 11500, 'NA', NULL, '$2y$10$BtY.CPcohwi.Wg3b30.G8e95dWaYLalRwRmymmyhEVIYAwnjUjRUC', NULL, '2020-04-23 15:18:08', '2020-04-23 15:18:08'),
(90, 0, 'Stephen', 'Mugwe Thuo', 89, 'NA', 'NA', 6000, 'NA', NULL, '$2y$10$RSnNpkcfJf1r1GPEfsTXKucv3g7mcTSIWFaX8WyTyCZVpv7jx.GfG', NULL, '2020-04-23 15:18:34', '2020-04-23 15:18:34'),
(91, 0, 'Simon', 'Kuria Thuo', 90, 'NA', 'NA', 11500, 'NA', NULL, '$2y$10$ja6jDrb1hfjhbbQKOvlNC.2aJAvBvmfMdb0DkeoGcp8meryDqtD0u', NULL, '2020-04-23 15:19:05', '2020-04-23 15:19:05'),
(92, 0, 'Ndung\'u', 'Mwangi Kiige', 91, 'NA', 'NA', 8500, 'NA', NULL, '$2y$10$NZsyC0eCf2/szJahwyph7uTVkeHwIpPnXg9vWXytc2GT31CQhX0Cy', NULL, '2020-04-23 15:19:40', '2020-04-23 15:19:40'),
(93, 0, 'Ephraim', 'Ndei', 92, 'NA', 'NA', 7000, 'NA', NULL, '$2y$10$0qynOLqLxe9887AdklDobeb8d.CLsBg1LBtQedXsyu/71RVAz7ieS', NULL, '2020-04-23 15:20:08', '2020-04-23 15:20:08'),
(94, 0, 'David', 'Mwangi Kimani', 93, 'NA', 'NA', 3500, 'NA', NULL, '$2y$10$CWgSSXy3xcQRv/L1zrGY9.x3UGdCP.fxgmQy.Gb3ZtO58lC6eVcGS', NULL, '2020-04-23 15:20:31', '2020-04-23 15:20:31'),
(95, 0, 'Zacharia', 'Muturi Chege', 94, 'NA', 'NA', 8000, 'NA', NULL, '$2y$10$KkkWEDsZJStQwVw/iCUX1.HgQdBoJjnW5EGGrBHG/Ka5Ec6MNnLJS', NULL, '2020-04-23 15:21:03', '2020-04-23 15:21:03'),
(96, 0, 'Stephen', 'Njoroge Mwangi', 95, 'NA', 'NA', 7000, 'NA', NULL, '$2y$10$yHob3Dj2fyfYP1c4J7bsQOW7HuwOrP/VaT7kve3c57LPYFZmOSh6q', NULL, '2020-04-23 15:21:34', '2020-04-23 15:21:34'),
(97, 0, 'John', 'Ken Nyaramba', 96, 'NA', 'NA', 11000, 'NA', NULL, '$2y$10$X7wGofYoS1VTM6Y5eJMEOeJRmwKgzwU17ny0sFIvjK6czHcNlDDyq', NULL, '2020-04-23 15:22:05', '2020-04-23 15:22:05'),
(98, 0, 'Habire', 'Chege', 97, 'NA', 'NA', 6000, 'NA', NULL, '$2y$10$AN4jDMSgp7hES0TuNJ6Px.xU6zKOrVJjzuU4aIa7XNpdejAqtI.Pu', NULL, '2020-04-23 15:22:34', '2020-04-23 15:22:34'),
(99, 0, 'Chege', 'Kembi', 98, 'NA', 'NA', 7500, 'NA', NULL, '$2y$10$OnF4TTlyF1A0YCAQF5sB6enQzWmYwsQmfVI.HCLBzVmn40CEzbnJS', NULL, '2020-04-23 15:23:01', '2020-04-23 15:23:01'),
(100, 0, 'George', 'Waruinge Kiarie', 99, 'NA', 'NA', 3500, 'NA', NULL, '$2y$10$30rUvNBHksSAQqYJipJH1epZlBoeSIko0OrYZ.jHVk9tVaKm.ZCz.', NULL, '2020-04-23 15:23:26', '2020-04-23 15:23:26'),
(101, 0, 'Kamau', 'Kiige', 100, 'NA', 'NA', 0, 'NA', NULL, '$2y$10$GIG8Fxf4Nlgt2gJyjYh/ZOlRjB4.SSZC4rMFN1A3Uv76zbwCYSnYa', NULL, '2020-04-23 15:23:55', '2020-04-23 15:23:55'),
(102, 0, 'Sam', '{ Insert Last name}', 101, 'NA', 'NA', 0, 'NA', NULL, '$2y$10$.zMdOqSgKPeXVUieHZgfIOYxIXqsrh0RpEPRK6KKlEmE1m2pKFVPm', NULL, '2020-04-23 15:24:33', '2020-04-23 15:24:33'),
(103, 0, 'Oscar', 'Waithaka Ngugi', 102, 'NA', 'NA', 11500, 'NA', NULL, '$2y$10$9/DH80C5J27a58PEF7CRie0kYYDF/kuKO.ssme3rk60adPQMGkxSi', NULL, '2020-04-23 15:25:04', '2020-04-23 15:25:04'),
(104, 0, 'James', 'Miano Njogu', 103, 'NA', 'NA', 9500, 'NA', NULL, '$2y$10$3Um/l3gSU3etYzNTHeyvvu7f5rZHzJLBQy879M9uZrbO1OsgNixA2', NULL, '2020-04-23 15:25:35', '2020-04-23 15:25:35'),
(105, 0, 'Eliud', 'Gachanja Kuria', 104, 'NA', 'NA', 5000, 'NA', NULL, '$2y$10$C7x2BiFn/fr93amkWNsQ9OqGxL02K8M6pC5CI/.lTdSNwnH.0oGnm', NULL, '2020-04-23 15:25:59', '2020-04-23 15:25:59'),
(106, 0, 'Julius', 'Ngugi Muraya', 105, 'NA', 'NA', 10000, 'NA', NULL, '$2y$10$a/JtnU20T2auk4pJsx2IE.l8pasoUgig6AESHgya05funTn75N22G', NULL, '2020-04-23 15:26:23', '2020-04-23 15:26:23'),
(107, 0, 'John', 'Githinji Kang\'ethe', 106, 'NA', 'NA', 5500, 'NA', NULL, '$2y$10$6UzrWa100j6iB.IZNEQCT.wdTbj4msLf7ZXnA7kTZaY4Deq4CJv6W', NULL, '2020-04-23 15:27:02', '2020-04-23 15:27:02'),
(108, 0, 'Julius', 'Kang\'ethe Githinji', 107, 'NA', 'NA', 12000, 'NA', NULL, '$2y$10$MVDNgjVkQoDqw5b96jhk2O3TKOFxLF3LKOIkoX3JMP7VR5BMawxGG', NULL, '2020-04-23 15:27:36', '2020-04-23 15:27:36'),
(109, 0, 'Jane', 'Wairimu Njoroge', 108, 'NA', 'NA', 8500, 'NA', NULL, '$2y$10$RUDUg/rD4OdYSXzlO2FM1ebHYOIzLXZfTHh6jR95BngkvjRmlsFlC', NULL, '2020-04-23 15:28:03', '2020-04-23 15:28:03'),
(110, 0, 'Mary', 'Wangechi Muriu', 109, 'NA', 'NA', 5500, 'NA', NULL, '$2y$10$HS1fyYVd0pLiEcneDtpJMu4Zoc1i2.360mPHYA2wnkxM5ow49pcI6', NULL, '2020-04-23 15:28:36', '2020-04-23 15:28:36'),
(111, 0, 'Peter', 'Mwangi Mwaragania', 110, 'NA', 'NA', 12000, 'NA', NULL, '$2y$10$z2e7B2Ix8O.5wl4FOFUD2utKlYi1HTGvC0hww.0xYMGnZQ7XQbENG', NULL, '2020-04-23 15:29:06', '2020-04-23 15:29:06'),
(112, 0, 'Zephania', 'Muraya Kabiri', 111, 'NA', 'NA', 8000, 'NA', NULL, '$2y$10$WDn.eNZ3GfiV4Xn8HO1WYOobaGWevhR3nnZMVFseHqGdUUOQk5p1e', NULL, '2020-04-23 15:29:33', '2020-04-23 15:29:33'),
(113, 0, 'Evanson', 'Maina', 112, 'NA', 'NA', 0, 'NA', NULL, '$2y$10$pw5saKAGmXONyJPtpn36g.BEpZDs3pRAjE9bqvB7DdoFGygiDHX5a', NULL, '2020-04-23 15:29:56', '2020-04-23 15:29:56'),
(114, 0, 'Florence', 'Cherono', 113, 'NA', 'NA', 0, 'NA', NULL, '$2y$10$QjuJ3iNnUHonPRoajs1hXO7OuHQtduWKEnv5l59QSlZfMVorFhRCq', NULL, '2020-04-23 15:30:19', '2020-04-23 15:30:19'),
(115, 0, 'Francis', 'Kimani Mwangi', 114, 'NA', 'NA', 10500, 'NA', NULL, '$2y$10$oOIMkqM4hbTJjjwE7q6RtOfxdDF80QiBur0R.aD3qKsY72YizRqJW', NULL, '2020-04-23 15:31:01', '2020-04-23 15:31:01'),
(116, 0, 'Peter', 'Ng\'ang\'a Kang\'ethe', 115, 'NA', 'NA', 9000, 'NA', NULL, '$2y$10$Kjvxmqi7fs4yJPImMUSzkuFVomx4prQqI.5hkGF.cXGKJIIAzo93e', NULL, '2020-04-23 15:31:30', '2020-04-23 15:31:30'),
(117, 0, 'Esbon', 'Njuguna Mwangi', 116, 'NA', 'NA', 10000, 'NA', NULL, '$2y$10$6VxsTQ175VDvPbbTjODqROj2oSzf9IpWfEyoqGE3zI2nK.XhAvQJS', NULL, '2020-04-23 15:32:04', '2020-04-23 15:32:04'),
(118, 0, 'Josphat', 'Irungu', 117, 'NA', 'NA', 8000, 'NA', NULL, '$2y$10$s.rCxLAg/hFKf8TLwo/DQ.ZbwkNTVsh8OzemrPllimfvQrRmHQYRy', NULL, '2020-04-23 15:32:32', '2020-04-23 15:32:32'),
(119, 0, 'Anthony', 'Wachira', 118, 'NA', 'NA', 9000, 'NA', NULL, '$2y$10$.izgEAjHA0rf8a8iXttJdeyj5Nu6FqRu.tTYmNv08qhiqLN4pKw/y', NULL, '2020-04-23 15:32:58', '2020-04-23 15:32:58'),
(120, 0, 'Wokabi', '{Insert Last name}', 119, 'NA', 'NA', 11500, 'NA', NULL, '$2y$10$2eBTYzlvRLizZZ1Rn1nRm.5GHglYGSJs5PSIAm71BlJp2A0Q4lhF2', NULL, '2020-04-23 15:33:35', '2020-04-23 15:33:35'),
(121, 0, 'Johnson', 'Karani Rigu', 120, 'NA', 'NA', 11500, 'NA', NULL, '$2y$10$2fQcoeFnrioW2mHyHT4mB.blgFXFMz9GZHcMHBWsZ69DAcBWeIdhS', NULL, '2020-04-23 15:34:18', '2020-04-23 15:34:18');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
