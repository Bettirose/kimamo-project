    <section class="content-header">
        <h1>
            User Details
            <small>General User info</small>
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Your Page Content Here -->
        <!-- /.content -->
        <div class="row">
            <div class="col-md-12">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body ">

                        <h3 class="profile-username">{{ $user->first_name }} {{ $user->other_names}}</h3>
                        <div class="col-md-4">
                            <strong>Email</strong> <p>{{ $user->email }}</p>
                            <strong>User Group</strong> <p>{{ $user->user_group }}</p>
                            <strong>Telephone</strong> <p>{{ $user->telephone }}</p>
                        </div>
                        <div class="col-md-4">
                            <strong>Pay Per Page</strong> <p>{{ $user->pay_per_page }}</p>
                            <strong>Active</strong> <p>{{ $user->active }}</p>
                            <strong>Registration Date</strong> <p>{{date('F d, Y', strtotime($user->created_at)) }}</p>
                        </div>
                        <div class="col-md-2">
                            {{link_to('user/edit/'.$user->id, "Update User Details", array('class' => 'btn btn-primary btn-block'))}}
                        </div>
                        <div class="col-md-2">
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body">
                    <h3 class="profile-username">Orders by {{ $user->first_name }} {{ $user->other_names}}</h3>

                    <table id="users" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th width="10px">Order #</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Order Date</th>
                            <th>Deadline</th>
                            <th>Pages</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->title}}</td>
                                <td>{!!$order->order_status()!!}</td>
                                <td>{{$order->created_at}}</td>
                                <td>{{date('F d, Y h:i A',strtotime($order->deadline)) }}   </td>
                                <td>{{$order->pages_number}}   </td>
                                <td> {{link_to('order/'.$order->id, "View Details")}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            </div>

        </div>
        <!-- /.row -->
    </section>
