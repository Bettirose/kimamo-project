<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Dashboard 3</title>

    <!-- Fontfaces CSS-->
    <link href={{{ asset("css/font-face.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/font-awesome-4.7/css/font-awesome.min.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/font-awesome-5/css/fontawesome-all.min.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/mdi-font/css/material-design-iconic-font.min.css") }}}  rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href={{{ asset("vendor/bootstrap-4.1/bootstrap.min.css") }}}  rel="stylesheet" media="all">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Vendor CSS-->
    <link href={{{ asset("vendor/animsition/animsition.min.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/wow/animate.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/css-hamburgers/hamburgers.min.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/slick/slick.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/select2/select2.min.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/perfect-scrollbar/perfect-scrollbar.css") }}}  rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href={{{ asset("css/theme.css") }}}  rel="stylesheet" media="all">
    
</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="/admin">
                            <!-- <img src={{{ asset("images/icon/logo-white.png") }}} alt="CoolAdmin" /> -->
                            <h3> KIMAMO SELF HELP GROUP </h3>
                        </a>
                    </div>
                    <div class="header__navbar">
                        <ul class="nav nav-tabs ">
                            <li class="has-sub">
                                <a href="/transactions">
                                    <i class="fas fa-tachometer-alt"></i>Transactions
                                    <span class="bot-line"></span>
                                </a>
                                
                            </li>
                            <li>
                                <a href="/members">
                                    <i class="fas fa-shopping-basket"></i>
                                    <span class="bot-line"></span>Members</a>
                            </li>
                           
                            <li class="has-sub">
                                <a href="#">
                                    <i class="fas fa-copy"></i>
                                    <span class="bot-line"></span>Send Messages</a>
                                
                            </li>
                            
                        </ul>
                    </div>
                    <div class="header__tool">
                        
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="image">
                                    <img src={{{ asset("images/icon/avatar-01.png") }}} alt="John Doe" />
                                </div>
                                <div class="content">
                                    <a class="js-acc-btn" href="#">{{ Auth::user()->firstname }}</a>
                                </div>
                               
                                <div class="account-dropdown js-dropdown">
                                    <div class="info clearfix">
                                        <div class="image">
                                            <a href="#">
                                                <img src={{{ asset("images/icon/avatar-01.png") }}} alt="John Doe" />
                                            </a>
                                        </div>
                                        <div class="content">
                                            <h5 class="name">
                                                <a href="#">{{ Auth::user()->firstname }}</a>
                                            </h5>
                                            <span class="email">{{ Auth::user()->email }}</span>
                                        </div>
                                    </div>
                                    <div class="account-dropdown__item">
                                        <a href="#">
                                            <i class="zmdi zmdi-account"></i>My Profile</a>
                                </div>
                                        <div class="account-dropdown__item">
                                            <a href="#">
                                                <i class="zmdi zmdi-account"></i>Change Password</a>
                                        </div>
                                        <div class="account-dropdown__footer">
                                        <a class="zmdi zmdi-power" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                           @csrf
                                        </form>  
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER DESKTOP-->

        <!-- HEADER MOBILE-->
        <header class="header-mobile header-mobile-2 d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="/admin">
                            <!-- <img src={{{ asset("images/icon/logo-white.png") }}} alt="CoolAdmin" /> -->
                            <h3> KIMAMO SELF HELP GROUP </h3>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <!-- <li class="has-sub">
                            <a class="js-arrow" href="/transactions">
                                <i class="fas fa-tachometer-alt"></i>Transactions

                        </li> -->
                        <li>
                            <a href="/transactions">
                                <i class="fas fa-chart-bar"></i>transactions</a>
                        </li>
                        <li>
                            <a href="/members">
                                <i class="fas fa-chart-bar"></i>Members</a>
                        </li>
                        <li>
                            <a href="table.html">
                                <i class="fas fa-table"></i>Send Messages</a>
                        </li>
                        
                    </ul>
                </div>
            </nav>
        </header>
        <div class="sub-header-mobile-2 d-block d-lg-none">
            <div class="header__tool">
                <div class="header-button-item has-noti js-item-menu">
                    
                </div>
                <div class="header-button-item js-item-menu">
                    
                </div>
                <div class="account-wrap">
                    <div class="account-item account-item--style2 clearfix js-item-menu">
                        <div class="image">
                            <img src={{{ asset("images/icon/avatar-01.png") }}} alt="John Doe" />
                        </div>
                        <div class="content">
                            <a class="js-acc-btn" href="#">{{ Auth::user()->firstname }}</a>
                        </div>
                        <div class="account-dropdown js-dropdown">
                            <div class="info clearfix">
                                <div class="image">
                                    <a href="#">
                                        <img src={{{ asset("images/icon/avatar-01.png") }}} alt="John Doe" />
                                    </a>
                                </div>
                                <div class="content">
                                    <h5 class="name">
                                        <a href="#">{{ Auth::user()->firstname }}</a>
                                    </h5>
                                    <span class="email">{{ Auth::user()->email }}</span>
                                </div>
                            </div>
                            <div class="account-dropdown__body">
                                <div class="account-dropdown__item">
                                        <a href="#">
                                            <i class="zmdi zmdi-account"></i>My Profile</a>
                                </div>
                                <div class="account-dropdown__item">
                                    <a href="#">
                                       
                                        <i class="zmdi zmdi-account"></i>Change Password</a>
                                </div>
                                
                                
                            </div>
                            <div class="account-dropdown__footer">
                                
                                    <a class="zmdi zmdi-power" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                           @csrf
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END HEADER MOBILE -->

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <!-- BREADCRUMB-->
            <section class="au-breadcrumb2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <span class="au-breadcrumb-span">You are here:</span>
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                            <a href="#">Home</a>
                                        </li>
                                        <li class="list-inline-item seprate">
                                            <span>/</span>
                                        </li>
                                        <li class="list-inline-item">Dashboard</li>
                                    </ul>
                                </div>
                                <form class="au-form-icon--sm" action="" method="post">
                                    <input class="au-input--w300 au-input--style2" type="text" placeholder="Search for datas &amp; reports...">
                                    <button class="au-btn--submit2" type="submit">
                                        <i class="zmdi zmdi-search"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END BREADCRUMB-->

            <!-- WELCOME-->
            <section class="welcome p-t-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title-4">Welcome back
                                <span>{{ Auth::user()->firstname }}!</span>
                            </h1>
                            <hr class="line-seprate">
                        </div>
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->

            
            
            <!-- DATA TABLE-->
            <section class="p-t-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35">Kimamo Members</h3>
                            <div class="table-data__tool">
                                <div class="table-data__tool-left">
                                    <div class="rs-select2--light rs-select2--md">
                                        <select class="js-select2" name="property">
                                            <option selected="selected">All Properties</option>
                                            <option value="">Option 1</option>
                                            <option value="">Option 2</option>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                    <div class="rs-select2--light rs-select2--sm">
                                        <select class="js-select2" name="time">
                                            <option selected="selected">Today</option>
                                            <option value="">3 Days</option>
                                            <option value="">1 Week</option>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                    <button class="au-btn-filter">
                                        <i class="zmdi zmdi-filter-list"></i>filters</button>
                                </div>
                                <div class="table-data__tool-right">
                                    <!-- <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                                        <i class="zmdi zmdi-plus"></i>add item</button> -->
                                        <a href = "register" class = "au-btn au-btn-icon au-btn--green au-btn--small"><i class="zmdi zmdi-plus"></i>Add a new member </a>
                                        
                                    <!-- <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                        <select class="js-select2" name="type">
                                            <option selected="selected">Export</option>
                                            <option value="">Option 1</option>
                                            <option value="">Option 2</option>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div> -->
                                </div>
                            </div>
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2">
                                    <thead>
                                        <tr>
                                            <th>
                                                <label class="au-checkbox">
                                                    <input type="checkbox">
                                                    <span class="au-checkmark"></span>
                                                </label>
                                            </th>
                                            
                                            <th>Member Name</th>
                                            <th>Member Number</th>
                                            <th>Phone Number</th>
                                            <th>Email</th>
                                            <th>Member Since</th>
                                            <th>Total Contributions</th>
                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($members as $member)
                                        <tr class="tr-shadow">
                                       
                                            <td>
                                                <label class="au-checkbox">
                                                    <input type="checkbox">
                                                    <span class="au-checkmark"></span>
                                                </label>
                                            </td>
                                            <td>
                                                {{$member->firstname}} {{$member->lastname}}
                                            </td>
                                            <td>
                                                {{$member->memberno}}
                                            </td>
                                            <td>
                                                {{$member->phoneno}}
                                            </td>
                                            <td>
                                                 {{$member->email}}
                                            </td>
                                            <td>
                                                 {{$member->created_at}}
                                            </td>
                                            <td>
                                            {{$member->totalcontribution}}
                                            </td>                                           
                                        </tr> @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END DATA TABLE-->

            <!-- COPYRIGHT-->
            <section class="p-t-60 p-b-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END COPYRIGHT-->
        </div>

    </div>


    <!-- Jquery JS-->
    <script src={{{ asset("vendor/jquery-3.2.1.min.js") }}}></script>
    <!-- Bootstrap JS-->
    <script src={{{ asset("vendor/bootstrap-4.1/popper.min.js") }}}></script>
    <script src={{{ asset("vendor/bootstrap-4.1/bootstrap.min.js") }}}></script>
    <!-- Vendor JS       -->
    <script src={{{ asset("vendor/slick/slick.min.js") }}}>
    </script>
    <script src={{{ asset("vendor/wow/wow.min.js") }}}></script>
    <script src={{{ asset("vendor/animsition/animsition.min.js") }}}></script>
    <script src={{{ asset("vendor/bootstrap-progressbar/bootstrap-progressbar.min.js") }}}>
    </script>
    <script src={{{ asset("vendor/counter-up/jquery.waypoints.min.js") }}}></script>
    <script src={{{ asset("vendor/counter-up/jquery.counterup.min.js") }}}>
    </script>
    <script src={{{ asset("vendor/circle-progress/circle-progress.min.js") }}}></script>
    <script src={{{ asset("vendor/perfect-scrollbar/perfect-scrollbar.js") }}}></script>
    <script src={{{ asset("vendor/chartjs/Chart.bundle.min.js") }}}></script>
    <script src={{{ asset("vendor/select2/select2.min.js") }}}>
    

    </script>
    <!-- Main JS-->
    <script src={{{ asset("js/mainy.js") }}}></script>
</body>

</html>
<!-- end document-->