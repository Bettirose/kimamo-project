<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Dashboard 3</title>

    <!-- Fontfaces CSS-->
    <link href={{{ asset("css/font-face.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/font-awesome-4.7/css/font-awesome.min.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/font-awesome-5/css/fontawesome-all.min.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/mdi-font/css/material-design-iconic-font.min.css") }}}  rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href={{{ asset("vendor/bootstrap-4.1/bootstrap.min.css") }}}  rel="stylesheet" media="all">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Vendor CSS-->
    <link href={{{ asset("vendor/animsition/animsition.min.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/wow/animate.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/css-hamburgers/hamburgers.min.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/slick/slick.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/select2/select2.min.css") }}}  rel="stylesheet" media="all">
    <link href={{{ asset("vendor/perfect-scrollbar/perfect-scrollbar.css") }}}  rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href={{{ asset("css/theme.css") }}}  rel="stylesheet" media="all">
    
</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="#">
                            <!-- <img src={{{ asset("images/icon/logo-white.png") }}} alt="CoolAdmin" /> -->
                            <h3 > KIMAMO SELF HELP GROUP</h3>
                        </a>
                    </div>
                    
                    <div class="header__tool">
                        
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="image">
                                    <img src={{{ asset("images/icon/avatar-01.png") }}} alt="John Doe" />
                                </div>
                                <div class="content">
                                    <a class="js-acc-btn" href="#">{{ Auth::user()->firstname }}</a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="info clearfix">
                                        <div class="image">
                                            <a href="#">
                                                <img src={{{ asset("images/icon/avatar-01.png") }}} alt="John Doe" />
                                            </a>
                                        </div>
                                        <div class="content">
                                            <h5 class="name">
                                                <a href="#">{{ Auth::user()->firstname }}</a>
                                            </h5>
                                            <span class="email">{{ Auth::user()->email }}</span>
                                        </div>
                                    </div>
                                    <div class="account-dropdown__item">
                                        <a href="#">
                                            <i class="zmdi zmdi-account"></i>My Profile</a>
                                </div>
                                        <div class="account-dropdown__item">
                                            <a href="#">
                                                <i class="zmdi zmdi-account"></i>Change Password</a>
                                        </div>
                                        <div class="account-dropdown__footer">
                                        <a class="zmdi zmdi-power" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                           @csrf
                                        </form>  
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER DESKTOP-->

        <!-- HEADER MOBILE-->
        <header class="header-mobile header-mobile-2 d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.html">
                            <!-- <img src={{{ asset("images/icon/logo-white.png") }}} alt="CoolAdmin" /> -->
                            <h3 > KIMAMO SELF HELP GROUP</h3>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
              
            </nav>
        </header>
        <div class="sub-header-mobile-2 d-block d-lg-none">
            <div class="header__tool">
                <div class="header-button-item has-noti js-item-menu">
                    
                </div>
                <div class="header-button-item js-item-menu">
                    
                </div>
                <div class="account-wrap">
                    <div class="account-item account-item--style2 clearfix js-item-menu">
                        <div class="image">
                            <img src={{{ asset("images/icon/avatar-01.png") }}} alt="John Doe" />
                        </div>
                        <div class="content">
                            <a class="js-acc-btn" href="#">{{ Auth::user()->firstname }}</a>
                        </div>
                        <div class="account-dropdown js-dropdown">
                            <div class="info clearfix">
                                <div class="image">
                                    <a href="#">
                                        <img src={{{ asset("images/icon/avatar-01.png") }}} alt="John Doe" />
                                    </a>
                                </div>
                                <div class="content">
                                    <h5 class="name">
                                        <a href="#">{{ Auth::user()->firstname }}</a>
                                    </h5>
                                    <span class="email">{{ Auth::user()->email }}</span>
                                </div>
                            </div>
                            <div class="account-dropdown__body">
                                <div class="account-dropdown__item">
                                        <a href="#">
                                            <i class="zmdi zmdi-account"></i>My Profile</a>
                                </div>
                                <div class="account-dropdown__item">
                                    <a href="#">
                                       
                                        <i class="zmdi zmdi-account"></i>Change Password</a>
                                </div>
                                
                                
                            </div>
                            <div class="account-dropdown__footer">
                                
                                    <a class="zmdi zmdi-power" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                           @csrf
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END HEADER MOBILE -->

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <!-- BREADCRUMB-->
            <section class="au-breadcrumb2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="au-breadcrumb-content">
                                <div class="au-breadcrumb-left">
                                    <span class="au-breadcrumb-span">You are here:</span>
                                    <ul class="list-unstyled list-inline au-breadcrumb__list">
                                        <li class="list-inline-item active">
                                            <a href="#">Home</a>
                                        </li>
                                        <li class="list-inline-item seprate">
                                            <span>/</span>
                                        </li>
                                        <li class="list-inline-item">Dashboard</li>
                                    </ul>
                                </div>
                                <form class="au-form-icon--sm" action="" method="post">
                                    <input class="au-input--w300 au-input--style2" type="text" placeholder="Search for datas &amp; reports...">
                                    <button class="au-btn--submit2" type="submit">
                                        <i class="zmdi zmdi-search"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END BREADCRUMB-->

            <!-- WELCOME-->
            <section class="welcome p-t-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title-4">Welcome back
                                <span>{{ Auth::user()->firstname }}!</span>
                            </h1>
                            <hr class="line-seprate">
                        </div>
                    </div>
                </div>
            </section>
            <!-- END WELCOME-->

            <!-- STATISTIC-->
            <section class="statistic statistic2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="statistic__item statistic__item--green">
                                <h2 class="number"> Ksh. {{ $totalcontribution->totalcontribution }}</h2>
                                <span class="desc">Total Cumulative Contributions</span>
                                <div class="icon">
                                    <i class="zmdi zmdi-account-o"></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <div class="statistic__item statistic__item--orange">
                                <h2 class="number">Ksh. {!!$revenueMonth!!}</h2>
                                <span class="desc">Contributions this month</span>
                                <div class="icon">
                                    <i class="zmdi zmdi-shopping-cart"></i>
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </section>
            <!-- END STATISTIC-->

            <!-- STATISTIC CHART-->
            <section class="statistic-chart">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35">statistics</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8 col-lg-8">
                            <!-- CHART-->
                            <div class="statistic-chart-1">
                                <h3 class="title-3 m-b-30">{{ $chart1->options['chart_title'] }}</h3>
                                <div class="chart-wrap">
                                   <canvas id="amount-chart"></canvas>
                                </div>
                                <div class="statistic-chart-1-note">
                                    <span class="big"></span>
                                    
                                </div>
                            </div>
                            <!-- END CHART-->
                        </div>
                        <div class="col-md-4 col-lg-4">
                            <!-- TOP CAMPAIGN-->
                            <div class="top-campaign">
                                <h3 class="title-3 m-b-30">top Contributors</h3>
                                <div class="table-responsive">
                                    <table class="table table-top-campaign">
                                    <thead>
                                    <tr>
                                        <th> Name</th>
                                        
                                    </tr>
                                    </thead>   
                                    <tbody>
                                        @foreach ($topcontributors as $contributor)
                                        <tr>
                                            <td>{!!$contributor->firstname!!} {!!$contributor->lastname!!}</td>
                                                 
                                        </tr>
                                        @endforeach 
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END TOP CAMPAIGN-->
                        </div>
                    </div>
                </div>
            </section>
            <!-- END STATISTIC CHART-->

            <!-- DATA TABLE-->
            <section class="p-t-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35">Your Transactions</h3>
                            <div class="table-data__tool">
                                <div class="table-data__tool-left">
                                    <div class="rs-select2--light rs-select2--md">
                                        <select class="js-select2" name="property">
                                            <option selected="selected">All Properties</option>
                                            <option value="">Option 1</option>
                                            <option value="">Option 2</option>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                    <div class="rs-select2--light rs-select2--sm">
                                        <select class="js-select2" name="time">
                                            <option selected="selected">Today</option>
                                            <option value="">3 Days</option>
                                            <option value="">1 Week</option>
                                        </select>
                                        <div class="dropDownSelect2"></div>
                                    </div>
                                    <button class="au-btn-filter">
                                        <i class="zmdi zmdi-filter-list"></i>filters</button>
                                </div>
                            </div>
                            <div class="table-responsive table-responsive-data2">
                                <table class="table table-data2">
                                    <thead>
                                        <tr>
                                            <th>
                                                <label class="au-checkbox">
                                                    <input type="checkbox">
                                                    <span class="au-checkmark"></span>
                                                </label>
                                            </th>
                                            <th>name</th>
                                            <th>member No</th>
                                            <th>transaction code</th>
                                            <th>phone number</th>
                                            <th>amount</th>
                                            <th>transaction time</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($contributions as $contribution)
                                        <tr class="tr-shadow">
                                       
                                            <td>
                                                <label class="au-checkbox">
                                                    <input type="checkbox">
                                                    <span class="au-checkmark"></span>
                                                </label>
                                            </td>
                                            
                                            <td>{{ $contribution->depositorname }}</td>
                                            <!-- <td>Lori Lynch</td> -->
                                            <td>
                                                <span class="block-email">{{ $contribution->memberno }}</span>
                                            </td>
                                            <td class="desc">{{ $contribution->referenceno }}</td>
                                            <td>{{ $contribution->transactionphoneno }}</td>
                                            <td>
                                                <span class="status--process">{{ $contribution->amount }}</span>
                                            </td>
                                            <td>{{ $contribution->transactiontime }}</td>
                                            
                                            <td>
                                                <!-- <div class="table-data-feature">
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Send">
                                                        <i class="zmdi zmdi-mail-send"></i>
                                                    </button>
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </button>
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </button>
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="More">
                                                        <i class="zmdi zmdi-more"></i>
                                                    </button>
                                                </div> -->
                                            </td>
                                           
                                        </tr> @endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END DATA TABLE-->

            <!-- COPYRIGHT-->
            <section class="p-t-60 p-b-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END COPYRIGHT-->
        </div>

    </div>


    <!-- Jquery JS-->
    <script src={{{ asset("vendor/jquery-3.2.1.min.js") }}}></script>
    <!-- Bootstrap JS-->
    <script src={{{ asset("vendor/bootstrap-4.1/popper.min.js") }}}></script>
    <script src={{{ asset("vendor/bootstrap-4.1/bootstrap.min.js") }}}></script>
    <!-- Vendor JS       -->
    <script src={{{ asset("vendor/slick/slick.min.js") }}}>
    </script>
    <script src={{{ asset("vendor/wow/wow.min.js") }}}></script>
    <script src={{{ asset("vendor/animsition/animsition.min.js") }}}></script>
    <script src={{{ asset("vendor/bootstrap-progressbar/bootstrap-progressbar.min.js") }}}>
    </script>
    <script src={{{ asset("vendor/counter-up/jquery.waypoints.min.js") }}}></script>
    <script src={{{ asset("vendor/counter-up/jquery.counterup.min.js") }}}>
    </script>
    <script src={{{ asset("vendor/circle-progress/circle-progress.min.js") }}}></script>
    <script src={{{ asset("vendor/perfect-scrollbar/perfect-scrollbar.js") }}}></script>
    <script src={{{ asset("vendor/chartjs/Chart.bundle.min.js") }}}></script>
    <script src={{{ asset("vendor/select2/select2.min.js") }}}>
    
    //{!! $chart1->renderJs() !!}
    <script>
    {!! $chart1->renderChartJsLibrary() !!}
    </script>
    </script>
    <!-- Main JS-->
    <script src={{{ asset("js/mainy.js") }}}></script>
    <script>
var ctx = document.getElementById('amount-chart');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels:  [
          @foreach ($contributionValues as $group => $result)
            "{{ $group }}",
          @endforeach
      ],
        datasets: [{
            label: '{{ $chart1->options['chart_title'] }}',
            data: [
            @foreach ($contributionValues as $group => $result)
            {!! $result !!},
            @endforeach
        ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            maintainAspectRatio: false,
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
console.log(myChart);
</script>
</body>

</html>
<!-- end document-->