<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Forms</title>

    <!-- Fontfaces CSS-->
    <link href={{{ asset("css/font-face.css") }}} rel="stylesheet" media="all">
    <link href={{{ asset("vendor/font-awesome-5/css/fontawesome-all.min.css") }}} rel="stylesheet" media="all">
    <link href={{{ asset("vendor/font-awesome-4.7/css/font-awesome.min.css") }}} rel="stylesheet" media="all">
    <link href={{{ asset("vendor/mdi-font/css/material-design-iconic-font.min.css") }}} rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href={{{ asset("vendor/bootstrap-4.1/bootstrap.min.css") }}} rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href={{{ asset("vendor/animsition/animsition.min.css") }}} rel="stylesheet" media="all">
    <link href={{{ asset("vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css") }}} rel="stylesheet" media="all">
    <link href={{{ asset("vendor/wow/animate.css") }}} rel="stylesheet" media="all">
    <link href={{{ asset("vendor/css-hamburgers/hamburgers.min.css") }}} rel="stylesheet" media="all">
    <link href={{{ asset("vendor/slick/slick.css") }}} rel="stylesheet" media="all">
    <link href={{{ asset("vendor/select2/select2.min.css") }}} rel="stylesheet" media="all">
    <link href={{{ asset("vendor/perfect-scrollbar/perfect-scrollbar.css") }}} rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href={{{ asset("css/theme.css") }}} rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER MOBILE-->
        <!-- HEADER MOBILE-->
        <header class="header-mobile header-mobile-2 d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="index.html">
                            <!-- <img src={{{ asset("images/icon/logo-white.png") }}} alt="CoolAdmin" /> -->
                            <h3> KIMAMO SELF HELP GROUP </h3>
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub">
                            <a class="js-arrow" href="/transactions">
                                <i class="fas fa-tachometer-alt"></i>Transactions</a>

                        </li>
                        <li>
                            <a href="/members">
                                <i class="fas fa-chart-bar"></i>Members</a>
                        </li>
                        <li>
                            <a href="">
                                <i class="fas fa-table"></i>Send Messages</a>
                        </li>
                        
                    </ul>
                </div>
            </nav>
        </header>
        <div class="sub-header-mobile-2 d-block d-lg-none">
            <div class="header__tool">
                <div class="header-button-item has-noti js-item-menu">
                    
                </div>
                <div class="header-button-item js-item-menu">
                    
                </div>
                <div class="account-wrap">
                    <div class="account-item account-item--style2 clearfix js-item-menu">
                        <div class="image">
                            <img src={{{ asset("images/icon/avatar-01.png") }}} alt="John Doe" />
                        </div>
                        <div class="content">
                            <a class="js-acc-btn" href="#">{{ Auth::user()->firstname }}</a>
                        </div>
                        <div class="account-dropdown js-dropdown">
                            <div class="info clearfix">
                                <div class="image">
                                    <a href="#">
                                        <img src={{{ asset("images/icon/avatar-01.png") }}} alt="John Doe" />
                                    </a>
                                </div>
                                <div class="content">
                                    <h5 class="name">
                                        <a href="#">{{ Auth::user()->firstname }}</a>
                                    </h5>
                                    <span class="email">{{ Auth::user()->email }}</span>
                                </div>
                            </div>
                            <div class="account-dropdown__body">
                                <div class="account-dropdown__item">
                                    <a href="#">
                                        <i class="zmdi zmdi-account"></i>Change Password</a>
                                </div>
                                
                            </div>
                            <div class="account-dropdown__footer">
                                
                                    <a class="zmdi zmdi-power" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                           @csrf
                                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END HEADER MOBILE -->
        <!-- END HEADER MOBILE-->

        <!-- PAGE CONTAINER-->
        <div class="page-content--bgf7">
            <!-- HEADER DESKTOP-->
                    <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="#">
                            <!-- <img src={{{ asset("images/icon/logo-white.png") }}} alt="CoolAdmin" /> -->
                            <h3> KIMAMO SELF HELP GROUP </h3>
                        </a>
                    </div>
                    <div class="header__navbar">
                        <ul class="list-unstyled">
                            <li class="has-sub">
                                <a href="/transactions">
                                    <i class="fas fa-tachometer-alt"></i>Transactions
                                    <span class="bot-line"></span>
                                </a>
                                
                            </li>
                            <li>
                                <a href="/members">
                                    <i class="fas fa-shopping-basket"></i>
                                    <span class="bot-line"></span>Members</a>
                            </li>
                            <!-- <li>
                                <a href="table.html">
                                    <i class="fas fa-trophy"></i>
                                    <span class="bot-line"></span>Users</a>
                            </li> -->
                            <li class="has-sub">
                                <a href="#">
                                    <i class="fas fa-copy"></i>
                                    <span class="bot-line"></span>Send Messages</a>
                                
                            </li>
                            
                        </ul>
                    </div>
                    <div class="header__tool">
                        
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="image">
                                    <img src={{{ asset("images/icon/avatar-01.png") }}} alt="John Doe" />
                                </div>
                                <div class="content">
                                    <a class="js-acc-btn" href="#">{{ Auth::user()->firstname }}</a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="info clearfix">
                                        <div class="image">
                                            <a href="#">
                                                <img src={{{ asset("images/icon/avatar-01.png") }}} alt="John Doe" />
                                            </a>
                                        </div>
                                        <div class="content">
                                            <h5 class="name">
                                                <a href="#">{{ Auth::user()->firstname }}</a>
                                            </h5>
                                            <span class="email">{{ Auth::user()->email }}</span>
                                        </div>
                                    </div>
                                        <div class="account-dropdown__item">
                                            <a href="#">
                                                <i class="zmdi zmdi-account"></i>Change Password</a>
                                        </div>
                                        <div class="account-dropdown__footer">
                                        <a class="zmdi zmdi-power" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                           @csrf
                                        </form>  
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER DESKTOP-->
            <!-- HEADER DESKTOP-->

            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">

                            <div class="col-md-10 text-left">
                                <div class="card">
                                    <div class="card-header">
                                        <strong>Edit a transaction</strong>
                                    </div>
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="card-body card-block">
                                        <form method="POST" action="{{route('transactions.update',$transaction->id)}}" enctype="multipart/form-data" class="form-horizontal">
                                       @csrf 
                                       @method('PUT')
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Name</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="text-input" value = "{{$transaction->depositorname}}" name="name" placeholder="Name" class="form-control">
                                                    <small class="form-text text-muted">Name of the person who made the transaction</small>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Transaction ID</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="text-input" value = "{{$transaction->referenceno}}"name="transactionid" placeholder="Transaction ID" class="form-control">
                                                    <small class="form-text text-muted">Transaction ID from the bank statement</small>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Phone Number</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="tel" id="text-input" value= "{{$transaction->transactionphoneno}}" name="phoneno" placeholder="Phone Number" class="form-control">
                                                    <small class="form-text text-muted">Phone Number used to send the transaction</small>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Amount</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="text-input" value = "{{$transaction->amount}}" name="amount" placeholder="Amount" class="form-control">
                                                    <small class="form-text text-muted">Amount sent to bank on the transaction</small>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input"  class=" form-control-label">Transaction Time</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="text-input" value = "{{$transaction->transactiontime}}" name="transactiontime" placeholder="Transaction Time" class="form-control">
                                                    <small class="form-text text-muted">Date and time the transaction was recorded in bank</small>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="text-input" class=" form-control-label">Member Number</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="text-input" value = "{{$transaction->memberno}}" name="memberno" placeholder="Member Number" class="form-control">
                                                    <small class="form-text text-muted">Member Number of the person who created the transaction</small>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="disabled-input" class=" form-control-label">Added by</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" id="disabled-input" name="addedby" value= "{{ Auth::user()->email }}" placeholder= "{{ Auth::user()->email }}" disabled="" class="form-control">
                                                </div>
                                            </div>
                                            
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="select" class=" form-control-label">Mode of Payment</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <select name="modeofpayment" id="select" value = "{{$transaction->depositmethod}}" class="form-control">
                                                        <option value="0">Please select</option>
                                                        <option value="1">MPESA to Paybill</option>
                                                        <option value="2">MPESA to Bank</option>
                                                        <option value="3">Direct deposit to Bank</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-9 text-center">
                                                <button type="submit" class="btn btn-primary btn-sm " value="Submit">
                                                    <i class="fa fa-dot-circle-o"></i> Submit
                                                </button>
                                                <button type="reset" class="btn btn-danger btn-sm ">
                                                    <i class="fa fa-ban"></i> Reset
                                                </button>
                                            </div>
                                                @if ($errors->any())
                                                    <div class="notification">
                                                        <ul>
                                                            @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                        </form>
                                    </div>
                                    
                                </div>
                            </div>
                           
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="copyright">
                                    <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Jquery JS-->
    <script src={{{ asset("vendor/jquery-3.2.1.min.js") }}}></script>
    <!-- Bootstrap JS-->
    <script src={{{ asset("vendor/bootstrap-4.1/popper.min.js") }}}></script>
    <script src={{{ asset("vendor/bootstrap-4.1/bootstrap.min.js") }}}></script>
    <!-- Vendor JS       -->
    <script src={{{ asset("vendor/slick/slick.min.js") }}}>
    </script>
    <script src={{{ asset("vendor/wow/wow.min.js") }}}></script>
    <script src={{{ asset("vendor/animsition/animsition.min.js") }}}></script>
    <script src={{{ asset("vendor/bootstrap-progressbar/bootstrap-progressbar.min.js") }}}>
    </script>
    <script src={{{ asset("vendor/counter-up/jquery.waypoints.min.js") }}}></script>
    <script src={{{ asset("vendor/counter-up/jquery.counterup.min.js") }}}>
    </script>
    <script src={{{ asset("vendor/circle-progress/circle-progress.min.js") }}}></script>
    <script src={{{ asset("vendor/perfect-scrollbar/perfect-scrollbar.js") }}}></script>
    <script src={{{ asset("vendor/chartjs/Chart.bundle.min.js") }}}></script>
    <script src={{{ asset("vendor/select2/select2.min.js") }}}>
    </script>

    <!-- Main JS-->
    <script src={{{ asset("js/mainy.js") }}}></script>

</body>

</html>
<!-- end document-->
